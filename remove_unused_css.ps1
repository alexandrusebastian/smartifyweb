#You need to have npm installed https://nodejs.org/en/download/
npm install -g purify-css
purifycss .\app\html_v1\static\css\materialize.css .\app\html_v1\templates\update.html --out .\app\html_v1\static\css\update.css --min
purifycss .\app\html_v1\static\css\materialize.css .\app\html_v1\templates\create.html --out .\app\html_v1\static\css\create.css --min
purifycss .\app\html_v1\static\css\materialize.css .\app\html_v1\templates\index.html --out .\app\html_v1\static\css\index.css --min