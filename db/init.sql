CREATE DATABASE Smartify;
use Smartify;

create table if not exists Version (
	VERSION INT NOT NULL
);

INSERT INTO Version
	(version) 
VALUES
	(1);

CREATE TABLE Articles (
	ID INT NOT NULL AUTO_INCREMENT,
	NAME TINYTEXT NOT NULL,
	DESCRIPTION TINYTEXT,
	CONTENT MEDIUMTEXT NOT NULL,
	AUTHOR INT NOT NULL, 
	CONSTRAINT PK_ARTICLES PRIMARY KEY(ID)
);

CREATE TABLE Users (
  id TEXT NOT NULL,
  name TEXT NOT NULL,
  email TEXT NOT NULL,
  profile_pic TEXT NOT NULL
);

CREATE TABLE Images (
  ID TINYTEXT NOT NULL,
  EXTENSION TINYTEXT NOT NULL,
  ARTICLE INT NOT NULL
);

CREATE TABLE Entities (
	ID INT NOT NuLL AUTO_INCREMENT,
	EntityType INT NOT NULL,
	NAME TINYTEXT NOT NULL,
	CONSTRAINT PK_ENTITITES PRIMARY KEY(ID)
);

CREATE TABLE EntityTypes (
	ID INT NOT NULL AUTO_INCREMENT,
	NAME TINYTEXT NOT NULL,
	CONSTRAINT PK_ENTITITES PRIMARY KEY(ID)
);

