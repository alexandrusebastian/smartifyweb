from flask_login import UserMixin
from image import Image
from common import connector, config

#Extend dict to make the class JSON serializable by default
#Also, the fields canbe accessed with the [] operator
class Article(dict):
	def __init__(self, id_, name, description, content, author):
		self.id_ = id_
		self.name = name
		self.description = description
		self.content = content
		self.author = author
		self.images = []
		dict.__init__(self)

	@staticmethod
	def create(name, description, content, author):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "INSERT INTO Articles (NAME, DESCRIPTION, CONTENT, AUTHOR) VALUES (%s, %s, %s, %s)"
		val = (name, description, content, author)
		cursor.execute(sql, val)        
		
		connection.commit()
		id_ =  cursor.lastrowid
		
		cursor.close()
		connection.close()

		return id_
		
	@staticmethod
	def read(article_id):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "SELECT NAME, DESCRIPTION, CONTENT, AUTHOR FROM Articles WHERE id = %s"
		val = (article_id,)
		
		cursor.execute(sql, val)
		article = cursor.fetchone()
				
		cursor.close()
		connection.close()

		if article is None:
			return None

		article = Article(
			id_=article_id, name=article[0], description=article[1], content=article[2], author=article[3]
		)
		article.images = Image.readAll(article.id_)
		return article

	@staticmethod
	def readContent(article_id):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "SELECT CONTENT FROM Articles WHERE id = %s"
		val = (article_id,)
		
		cursor.execute(sql, val)
		article = cursor.fetchone()
				
		cursor.close()
		connection.close()

		if article is None:
			return None
			
		return article[0]

	@staticmethod
	def readAll():
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		cursor.execute('SELECT ID, NAME, DESCRIPTION, CONTENT, AUTHOR FROM Articles')
		results = cursor.fetchall()
		
		cursor.close()
		connection.close()
		
		articleList=[]
		for result in results:
			article = Article(
				id_=result[0], 
				name=result[1], 
				description=result[2], 
				content=result[3], 
				author=result[4]
			)
			article.images = Image.readAll(article.id_)
			articleList.append(article)
		
		return articleList
	
	@staticmethod
	def update(id_, name, description, content, author):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "UPDATE Articles SET NAME=%s, DESCRIPTION=%s, CONTENT=%s, AUTHOR=%s"
		val = (name, description, content, author)
		cursor.execute(sql, val)        
		
		connection.commit()
		id_ =  cursor.lastrowid
		
		cursor.close()
		connection.close()

		return id_

	@staticmethod
	def delete(id_):
		connection = connector.connect(**config)
		cursor = connection.cursor()

		sql = "DELETE FROM Articles WHERE ID=%s"
		val = (id_,)
		cursor.execute(sql, val)
		
		connection.commit()    
	   
		cursor.close()
		connection.close()

		Image.deleteForArticle(id_)

		return id_