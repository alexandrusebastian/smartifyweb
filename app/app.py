#!flask/bin/python
# -*- coding: utf-8 -*-

from common import app
import login

#Get the blueprint objects
from html_client.routes.html_v1 import html_v1
from html_client.routes.html_v2 import html_v2
from json_client.routes.json_v1 import json_v1

#Register blueprints
app.register_blueprint(html_v1, url_prefix="/html_v1")
app.register_blueprint(html_v2, url_prefix="/html_v2")
app.register_blueprint(json_v1, url_prefix="/json_v1")

if __name__ == '__main__':
    app.run(
        debug=True, 
        host='0.0.0.0', 
        port=5000, 
        threaded=True 
        #,ssl_context="adhoc"
    )
