from common import *
from article import Article
from werkzeug.utils import secure_filename
from image import Image
from flask import render_template, url_for, redirect

html_v2 = Blueprint('html_v2', __name__, template_folder='../templates', static_folder='../static', static_url_path='/static/')

UPLOAD_FOLDER = 'html_client/static/images'

@app.errorhandler(404)
def not_found(error):
	result = {}
	result['result'] = 'Error'
	result['message'] = 'Not found'
	return make_response(jsonify(result), 404)

#Front page
@html_v2.route('/index', methods=['GET'])
def renderIndexPage():
	articles = Article.readAll()
	return render_template('html_v2/index.html', articles = articles) 

#---------------------------------------------------------------#
#-------------------------Create--------------------------------#
#---------------------------------------------------------------#

#Default empty create page. The user can start creating articles here
@html_v2.route('/create', methods=['GET'])
def renderCreatePage():
	return render_template('create.html') 

#After submitting, the request gets here for processing
@html_v2.route('/create', methods=['POST'])
def createArticleFromHtml():
	name = request.form.get('Name')
	description = request.form.get('Description')
	content = request.form.get('htmlContent')
	newId = Article.create(name, description, content, 1)
	
	Image.upload_files(request, UPLOAD_FOLDER, newId)
	return redirect(url_for('html_v2.renderEditPage', article_id=newId))

#---------------------------------------------------------------#
#-------------------------Update--------------------------------#
#---------------------------------------------------------------#

#The default update page
@html_v2.route('/edit/update', methods=['POST'])
def updateArticleFromHtml():
	id_ = request.form.get('ID')
	name = request.form.get('Name')
	description = request.form.get('Description')
	content = request.form.get('htmlContent')
	Article.update(id_, name, description, content, 1)
	print(request.form)
	a = Article.read(id_)
	return render_template('update.html', article=a)

#Get from database the HTML content of the article that is displayed in the iframe
@html_v2.route('/edit/content/<article_id>', methods=['GET'])
def getArticleContent(article_id):
	content = Article.readContent(article_id)
	return content

#Edit page for a given article
@html_v2.route('/edit/<article_id>', methods=['GET'])
def renderEditPage(article_id):
	a = Article.read(article_id)
	return render_template('update.html', article=a)

#Update image
@html_v2.route('/edit/image/<image_id>', methods=['POST'])
def editImage(image_id):
	Image.updateImage(image_id, image)
	return make_response("Success")

#---------------------------------------------------------------#
#-------------------------Delete--------------------------------#
#---------------------------------------------------------------#

#Delete image from database and disk
@html_v2.route('/delete_image/<image_id>', methods=['GET'])
def deleteImage(image_id):
	Image.delete(image_id)
	return make_response("Success")

#Delete entire article
@html_v2.route('/delete/<article_id>', methods=['GET'])
def deleteArticle(article_id):
	Article.delete(article_id)
	return make_response("Success")


#---------------------------------------------------------------#
#-------------------------Miscelanious--------------------------#
#---------------------------------------------------------------#

@html_v2.route('/status', methods=['GET'])
def status():
	status = {"status":"online"}    
	return make_response(jsonify(status))
