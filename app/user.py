from flask_login import UserMixin

from common import connector, config

class User(UserMixin):
    def __init__(self, id_, name, email, profile_pic):
        self.id = id_
        self.name = name
        self.email = email
        self.profile_pic = profile_pic

    @staticmethod
    def get(user_id):
        connection = connector.connect(**config)
        cursor = connection.cursor()
        
        sql = "SELECT * FROM Users WHERE id = %s"
        val = (user_id,)
        
        cursor.execute(sql, val)
        user = cursor.fetchone()
                
        cursor.close()
        connection.close()

        if user is None:
            return None

        user = User(
            id_=user[0], name=user[1], email=user[2], profile_pic=user[3]
        )
        return user

    @staticmethod
    def create(id_, name, email, profile_pic):
        connection = connector.connect(**config)
        cursor = connection.cursor()
        
        sql = "INSERT INTO Users (ID, NAME, EMAIL, PROFILE_PIC) VALUES (%s, %s, %s, %s)"
        val = (id_, name, email, profile_pic)
        cursor.execute(sql, val)        
        
        connection.commit()
        rowId =  cursor.lastrowid
        
        cursor.close()
        connection.close()

        return rowId
        
    def readDbUsers():
        connection = connector.connect(**config)
        cursor = connection.cursor()
        
        cursor.execute('SELECT * FROM Users')
        results = cursor.fetchall()
        
        cursor.close()
        connection.close()

        return results