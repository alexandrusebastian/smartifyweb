from common import *
from user import User
from article import Article

json_v1 = Blueprint('json_v1', __name__)

@app.errorhandler(404)
def not_found(error):
	result = {}
	result['result'] = 'Error'
	result['message'] = 'Not found'
	return make_response(jsonify(result), 404)   

@json_v1.route('/articles')
def readArticles():
    return make_response(str(Article.readAll()))
    
@json_v1.route('/users')
def readUsers():
    return make_response(jsonify(User.readDbUsers()))
    
@json_v1.route('/article')
def createArticle():
    return make_response(str(Article.create("1", "2", "3", 4)))