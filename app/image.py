import os
from common import connector, config, app
from PIL import Image as PilImage

#Extend dict to make the class JSON serializable by default
#Also, the fields canbe accessed with the [] operator
class Image(dict):
	ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

	def __init__(self, id_, extension, article):
	   dict.__init__(
			self,
			id_ = id_,
			extension = extension,
			article = article
		)

	@staticmethod
	def create(id_, extension, article):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "INSERT INTO Images (ID, EXTENSION, ARTICLE) VALUES (%s, %s, %s)"
		val = (id_, extension, article)
		cursor.execute(sql, val)        
		
		connection.commit()
		
		cursor.close()
		connection.close()

		return id_
		
	@staticmethod
	def read(article_id):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql = "SELECT ID, EXTENSION FROM Images WHERE ARTICLE = %s"
		val = (article_id,)
		
		cursor.execute(sql, val)
		image = cursor.fetchone()
				
		cursor.close()
		connection.close()

		if image is None:
			return None

		image = Image(
			id_=image[0], extension=image[1], article=article_id
		)
		return article

	@staticmethod
	def readAll(article_id):
		connection = connector.connect(**config)
		cursor = connection.cursor()
		
		sql ='SELECT ID, EXTENSION FROM Images WHERE ARTICLE = %s'
		val = (article_id,)
		
		cursor.execute(sql, val)
		results = cursor.fetchall()
		
		cursor.close()
		connection.close()
		
		imageList=[]
		for result in results:
			image = Image(
				id_=result[0], extension=result[1], article=article_id
			)
			imageList.append(image)
		
		return imageList
	
	@staticmethod
	def update(image_id, file):
		if(Image.allowed_file(file.filename)):
			extension = "webp"
			name = image_id
			filename = name + "." + extension
			path = os.path.join(app.root_path, upload_folder)

			im = PilImage.open(file).convert("RGB")
			im.save(os.path.join(path, filename), extension, lossless=False, method=6)
			Image.save_thumbnails(im, path, name, extension)

		return "Success"

	@staticmethod
	def delete(id_):
		#Delete from disk
		connection = connector.connect(**config)
		cursor = connection.cursor()

		sql = "DELETE FROM Images WHERE ID=%s"
		val = (id_,)
		cursor.execute(sql, val)
		
		connection.commit()    
	   
		cursor.close()
		connection.close()

		return id_

	@staticmethod
	def deleteForArticle(article_id):
		connection = connector.connect(**config)
		cursor = connection.cursor()

		sql = "DELETE FROM Images WHERE ARTICLE=%s"
		val = (article_id,)
		cursor.execute(sql, val)
		
		connection.commit()    
	   
		cursor.close()
		connection.close()

		return

	@staticmethod
	def allowed_file(filename):
		return '.' in filename and filename.rsplit('.', 1)[1].lower() in Image.ALLOWED_EXTENSIONS

	@staticmethod
	def upload_files(request, upload_folder, article_id):
		# check if the post request has the file part
		if 'files' not in request.files:
			return make_response(jsonify("No file part"))
		files = request.files.getlist('files')
		count = 0
		path = os.path.join(app.root_path, upload_folder)
		
		for file in files:
			if(Image.allowed_file(file.filename)):
				count+=1
				extension = "webp"
				name = str(article_id) + "_" + str(count)
				filename = name + "." + extension

				im = PilImage.open(file).convert("RGB")
				im.save(os.path.join(path, filename), extension, lossless=False, method=6)
				Image.create(name, extension, article_id)

				Image.save_thumbnails(im, path, name, extension)

		return "Success"

	@staticmethod
	def save_thumbnails(im, path, name, extension):
		im.thumbnail((1920, 1920))
		filename = name + "_1920." + extension
		im.save(os.path.join(path, filename), extension, lossless=False, method=6)

		im.thumbnail((1536, 1536))
		filename = name + "_1536." + extension
		im.save(os.path.join(path, filename), extension, lossless=False, method=6)

		im.thumbnail((1366, 1366))
		filename = name + "_1366." + extension
		im.save(os.path.join(path, filename), extension, lossless=False, method=6)

		im.thumbnail((780, 780))
		filename = name + "_780." + extension
		im.save(os.path.join(path, filename), extension, lossless=False, method=6)

		im.thumbnail((640, 640))
		filename = name + "_640." + extension
		im.save(os.path.join(path, filename), extension, lossless=False, method=6)